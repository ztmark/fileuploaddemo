<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <title>index</title>
  </head>
  
  <body>
    <h1>Files</h1>
    <div>
    	<ul>
    		<c:forEach var="f" items="${files }">
    			<li>${f.filename } ---  <a href="${pageContext.request.contextPath }/DownloadFileServlet?id=${f.id}">下载</a></li>
    		</c:forEach>
    	</ul>
    </div>
    <div>
    	<form action="${pageContext.request.contextPath }/UploadFileServlet" method="post" enctype="multipart/form-data">
    		<input type="file" name="file" />
    		<button type="submit">Upload</button>
    	</form>
    	<div>${message }</div>
    </div>
  </body>
</html>
