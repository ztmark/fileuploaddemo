package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBUtil {
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/uploadfiles";
	private static String username = "root";
	private static String password = "root";
	
	private DBUtil(){}
	
	static {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void close(Connection conn, Statement st, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if(st != null) {
					st.close();
				}
			} catch (SQLException e2) {
				throw new RuntimeException(e2);
			} finally {
				try {
					if(conn != null) {
						conn.close();
					}
				} catch (SQLException e3) {
					throw new RuntimeException(e3);
				}
			}
		}
	}
}
