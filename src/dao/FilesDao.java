package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import utils.DBUtil;

import bean.FileBean;

public class FilesDao {
	
	public List<FileBean> getAllFiles() {
		List<FileBean> files = new ArrayList<>();
		
		String sql = "SELECT * FROM files";
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				String filename = rs.getString("filename");
				String path = rs.getString("path");
				FileBean f = new FileBean(id, filename, path);
				files.add(f);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			DBUtil.close(conn, st, rs);
		}
		
		return files;
	}
	
	public FileBean getFile(int id) {
		FileBean file = new FileBean();
		
		String sql = "SELECT * FROM files where id = ?";
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getConnection();
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				String filename = rs.getString("filename");
				String path = rs.getString("path");
				file.setId(id);
				file.setFilename(filename);
				file.setPath(path);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			DBUtil.close(conn, st, rs);
		}
		
		return file;
	}
	
	public void uploadFile(FileBean f) {
		String sql = "INSERT INTO files(filename,path) values(?,?)";
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getConnection();
			st = conn.prepareStatement(sql);
			st.setString(1, f.getFilename());
			st.setString(2, f.getPath());
			st.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			DBUtil.close(conn, st, rs);
		}
	}
	
}
