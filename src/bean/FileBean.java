package bean;


public class FileBean {
	private int id;
	private String filename ;
	private String path;
	
	public FileBean(int id, String filename, String path) {
		this.id = id;
		this.filename = filename;
		this.path = path;
	}
	
	public FileBean(String filename, String path) {
		this.filename = filename;
		this.path = path;
	}
	
	public FileBean() {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	
}
