package bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import dao.FilesDao;

public class DownloadFileServlet extends HttpServlet {

	private FilesDao dao = new FilesDao();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		FileBean f = dao.getFile(Integer.valueOf(id));
		File file = new File(f.getPath()+File.separator+f.getFilename());
		if(!file.exists()) {
			request.setAttribute("message", "文件已删除！");
			request.getRequestDispatcher("/message.jsp");
			return;
		}
		response.setHeader("content-disposition", "attachment;filename="+ URLEncoder.encode(file.getName(),"utf-8"));
		int len;
		byte[] buffer = new byte[1024];
		InputStream in = new FileInputStream(file);
		OutputStream out = response.getOutputStream();
		while((len=in.read(buffer))>0){
			out.write(buffer, 0, len);
		}
		in.close();
		out.close();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
