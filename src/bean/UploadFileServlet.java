package bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dao.FilesDao;

public class UploadFileServlet extends HttpServlet {

	private FilesDao dao = new FilesDao();

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if(!ServletFileUpload.isMultipartContent(request)) {
			request.setAttribute("message", "不支持的操作！");
			request.getRequestDispatcher("/ListFilesServlet").forward(request, response);
		}
		
		try {
			String path = request.getServletContext().getRealPath("/WEB-INF/files");
			FileBean file = upload(request, path);
			dao.uploadFile(file);
			request.setAttribute("message", "上传成功");
			request.getRequestDispatcher("/ListFilesServlet").forward(request, response);
		} catch (FileSizeLimitExceededException e) {
			request.setAttribute("message", "文件过大！");
			request.getRequestDispatcher("/ListFilesServlet").forward(request, response);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private FileBean upload(HttpServletRequest request, String path) throws FileSizeLimitExceededException {
		FileBean file = new FileBean();
		
		try {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload ful = new ServletFileUpload(factory);
			ful.setFileSizeMax(1024*1024*10); //设置最大的上传文件大小
			ful.setHeaderEncoding("utf-8");
			List<FileItem> items = ful.parseRequest(request);
			for (FileItem item : items) {
				if(!item.isFormField()) { // 是文件格式
					String filename = item.getName();
					file.setFilename(filename);
					file.setPath(path);
					OutputStream output = new FileOutputStream(new File(path + File.separator + filename));
					InputStream in = item.getInputStream();
					int len;
					byte[] buffer = new byte[1024];
					while( ( len=in.read(buffer) ) > 0 ) {
						output.write(buffer,0,len);
					}
					in.close();
					output.close();
				}
			}
		} catch (FileUploadBase.FileSizeLimitExceededException e) {
			throw e;
		} catch (FileUploadException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return file;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
